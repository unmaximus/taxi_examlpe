<?php

namespace app\modules\api;

use yii\base\BootstrapInterface;
use yii\base\Module;

/**
 * api module definition class
 */
class ApiModule extends Module implements BootstrapInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\api\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

	public function bootstrap($app) {
		if ($app instanceof \yii\console\Application) {
			$this->controllerNamespace = 'app\modules\api\commands';
		}
	}
}
