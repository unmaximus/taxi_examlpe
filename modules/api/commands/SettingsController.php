<?php


namespace app\modules\api\commands;

use app\components\CommandController;
use app\models\ar\Project;

class SettingsController extends CommandController
{
	public function actionGenerateToken($project)
	{
		$model = Project::getOrCreate($project);
		$model->token = \Yii::$app->security->generateRandomString();
		$model->save();
		$this->inputString($model->token);
	}
}