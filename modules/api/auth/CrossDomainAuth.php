<?php

namespace app\modules\api\auth;

use app\models\ar\Project;
use app\models\User;
use app\modules\system\helpers\ArrayHelper;


/**
 * Class QueryParamAuthWithPost
 * @package app\modules\api\auth
 */
class CrossDomainAuth extends \yii\filters\auth\QueryParamAuth
{
	/**
	 * @param $user
	 * @param $request
	 * @param $response
	 *
	 * @return User|\yii\web\IdentityInterface|null
	 * @throws \yii\base\Exception
	 */
	public function authenticate($user, $request, $response)
    {
	    $authHeader = $request->getHeaders()->get('Authorization');

	    if ($authHeader !== null && preg_match('/^Bearer\s+(.*?)$/', $authHeader, $matches)) {
		    $accessToken = $matches[1];

	        if (is_string($accessToken) && $project = $this->getProject($accessToken)) {
		        return User::getCompanyBot($project, 0);
	        }
	    }

        return null;
    }

	/**
	 * @param $token
	 *
	 * @return string|null
	 */
	protected function getProject($token)
    {
    	$model = Project::find()->select(['name'])
                     ->where(['token'=>$token])
                     ->one();
	    return ArrayHelper::getValue($model, 'name');
    }
}