<?php

namespace app\modules\api\controllers;

use app\filters\HttpBearerAuth;
use app\modules\api\auth\CrossDomainAuth;
use yii\base\InvalidConfigException;
use yii\filters\auth\CompositeAuth;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\ContentNegotiator;

class BaseController extends \yii\rest\Controller
{
	public $modelClass;
	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'contentNegotiator' => [
				'class' => ContentNegotiator::class,
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter' => [
				'class' => VerbFilter::class,
				'actions' => $this->verbs(),
			],
			'authenticator' => [
				'class' => CompositeAuth::class,
				'authMethods' => [
					[
						'class' => CrossDomainAuth::class,
					],
				],
			],
		];
	}
}
