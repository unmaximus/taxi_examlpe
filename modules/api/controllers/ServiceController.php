<?php

namespace app\modules\api\controllers;

use app\models\ar\Company;
use app\models\ar\Project;
use app\modules\system\helpers\ArrayHelper;
use app\modules\system\responses\BaseResponse;
use Yii;
use app\controllers\BaseController as AppBaseController;

class ServiceController extends AppBaseController
{
	public $modelClass = 'app\models\ar\Company';

	/**
	 * @return BaseResponse
	 */
	public function actionUpdatePredictableDate()
	{
		try {
			$params = Yii::$app->request->post();

			if (!isset($params['data']) or !isset($params['site_id'])) {
				return BaseResponse::errorResponse('Неверные данные');
			}

			$ids = ArrayHelper::getColumn($params['data'], 'id');
			$data = ArrayHelper::index($params['data'], 'id');

			$project_id = Project::getOrCreate($params['site_id'])->id;

			$companies = Company::find()->where([
				'inner_id' => $ids,
				'project_id' => $project_id,
			])->all();

			foreach ($companies as $company) {

				if (isset($data[ $company->inner_id ])) {

					$row = $company->updateAttributes(['predictable_date_of_blocking' => $data[ $company->inner_id ]['predictable_date_of_blocking']]);
				}
			}

		} catch (\Exception $e) {
			\Yii::error([
				'message' => $e->getMessage(),
				'label' => 'Произошла ошибка при обновлении прогнозируемой даты блокировки',
			]);
			return BaseResponse::errorResponse($e);
		}

		return BaseResponse::successResponse('Успешно');
	}
}
