<?php

namespace app\modules\api\controllers;

use app\modules\dashboard\models\ar\CompanyLimitRule;
use app\models\ar\Project;
use app\modules\system\responses\BaseResponse;
use Yii;
use yii\web\NotFoundHttpException;
use app\models\ar\Company;
use app\controllers\BaseController as AppBaseController;

class CompanyRuleController extends AppBaseController
{
	public $modelClass = 'app\models\ar\CompanyLimitRule';

	/**
	 * @return BaseResponse
	 */
	public function actionCreate()
	{
		$params = Yii::$app->request->post();

		$company_id = Company::find()
		                     ->select(Company::tableName() . '.id')->joinWith('project')
		                     ->where([
			                     Company::tableName() . '.inner_id' => $params['container_id'],
			                     Project::tableName() . '.name' => $params['site_id'],
		                     ])->scalar();

		if (empty($company_id)) {
			return BaseResponse::errorResponse('Отсутсвует запись о данной компании.');
		}
		$params['company_id'] = $company_id;

		$model = new CompanyLimitRule(['scenario' => CompanyLimitRule::SCENARIO_API]);
		$model->detachBehavior('sendToProject');

		if ($model->load($params, '') && $model->save()) {
			return BaseResponse::successResponse('Успешно');
		} else {
			return BaseResponse::errorResponse($model->getFirstErrors());
		}
	}

	/**
	 * @return BaseResponse
	 * @throws NotFoundHttpException
	 */
	public function actionUpdate()
	{
		$params = Yii::$app->request->post();

		$model = $this->findModel($params);
		if ($model->load($params, '') && $model->save()) {
			return BaseResponse::successResponse('Успешно');
		} else {
			return BaseResponse::errorResponse($model->getFirstErrors());
		}
	}

	/**
	 * @return BaseResponse
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete()
	{
		$params = Yii::$app->request->post();

		$model = $this->findModel($params);

		if ($model->delete()) {
			return BaseResponse::successResponse('Успешно');
		} else {
			return BaseResponse::errorResponse('Ошибка удаления');
		}
	}

	/**
	 * @param $params
	 *
	 * @return CompanyLimitRule|array|\yii\db\ActiveRecord|null
	 * @throws NotFoundHttpException
	 */
	protected function findModel($params)
	{
		if (empty($params['container_id']) or empty($params['inner_id']) or empty($params['site_id'])) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}

		$model = CompanyLimitRule::find()->alias('clr')
		                         ->leftJoin(Company::tableName() . ' c', 'c.id = clr.company_id')
		                         ->leftJoin(Project::tableName() . ' p', 'p.id = c.project_id')
		                         ->andWhere([
			                         'c.inner_id' => $params['container_id'],
			                         'clr.inner_id' => $params['inner_id'],
			                         'p.name' => $params['site_id'],
		                         ])
		                         ->one();

        if ($model == null) {
	        throw new NotFoundHttpException('The requested page does not exist.');
		}

		$model->detachBehavior('sendToProject');

		return $model;
	}
}
