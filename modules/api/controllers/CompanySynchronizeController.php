<?php

namespace app\modules\api\controllers;

use app\models\ar\Company;
use app\modules\dashboard\models\ar\CompanyBalanceOperation;
use app\modules\dashboard\models\ar\CompanyLimitRule;
use app\modules\dashboard\models\ar\CompanyWithdrawalRule;
use app\models\ar\Project;
use app\modules\system\helpers\ArrayHelper;
use app\modules\system\responses\BaseResponse;
use Yii;
use app\controllers\BaseController as AppBaseController;


class CompanySynchronizeController extends AppBaseController
{
	public $modelClass = 'app\models\ar\Company';

	/**
	 * @return BaseResponse
	 */
	public function actionIndex()
	{
		try {
			$params = Yii::$app->request->post();

			if (!isset($params['data']) or !isset($params['site_id'])) {
				return BaseResponse::errorResponse('Неверные данные');
			}

			$data = ArrayHelper::index($params['data'], 'company.container_id');

			$project = Project::getOrCreate($params['site_id']);
			$project_id = $project->id;

			// Обновление компаний
			foreach ($data as $companyData) {
				$row = $companyData['company'];
				$row['inner_id'] = $row['container_id'];
				$row['project_id'] = $project_id;
				$row['is_synchronized'] = true;
				unset($row['id']);

				$company = Company::find()
				                  ->where(['inner_id' => $row['inner_id']])
				                  ->andWhere(['project_id' => $project_id])
				                  ->orderBy('id')
				                  ->one();
				if (empty($company)) {
					$company = new Company();
				}
				$company->detachBehavior('sendToProject');

				if ($company->load($row, '') and $company->save()) {

					// Обновление правил
					foreach ($companyData['withdrawalRule'] as $key => $rowRule) {
						$rowRule['inner_id'] = $rowRule['id'];
						$rowRule['company_id'] = $company->id;
						unset($rowRule['id']);

						$rule = CompanyWithdrawalRule::find()
						                             ->where(['inner_id' => $rowRule['inner_id']])
						                             ->andWhere(['company_id' => $company->id])
						                             ->orderBy('id')
						                             ->one();

						if (empty($rule)) {
							$rule = new CompanyWithdrawalRule();
						}
						$rule->detachBehavior('sendToProject');

						if ($rule->load($rowRule, '') and $rule->save()) {
							$companyData['withdrawalRule'][$key]['id_api'] = $rule->id;
						} else {
							\Yii::error([
								'message' => $rule->getFirstErrors(),
								'label' => 'Произошла ошибка при синхронизации данных (правило) с ' . $params['site_id'] . ' компании ' . $row['name'],
							]);
						}
					}
					$withdrawalRule = ArrayHelper::index($companyData['withdrawalRule'], 'id');

					// Обновление лимитов
					foreach ($companyData['limitRule'] as $rowLimit) {
						$rowLimit['inner_id'] = $rowLimit['id'];
						$rowLimit['company_id'] = $company->id;
						unset($rowLimit['id']);

						$limit = CompanyLimitRule::find()
						                         ->where(['inner_id' => $rowLimit['inner_id']])
						                         ->andWhere(['company_id' => $company->id])
						                         ->orderBy('id')
						                         ->one();
						if (empty($limit)) {
							$limit = new CompanyLimitRule();
						}
						$limit->setScenario(CompanyLimitRule::SCENARIO_API);
						$limit->detachBehavior('sendToProject');

						if ($limit->load($rowLimit, '') and $limit->save()) {
							//
						} else {
							\Yii::error([
								'message' => $limit->getFirstErrors(),
								'label' => 'Произошла ошибка при синхронизации данных (лимит) с ' . $params['site_id'] . ' компании ' . $row['name'],
							]);
						}
					}

					// Обновление операций по балансу компании
					foreach ($companyData['balanceOperation'] as $rowOperation) {
						if (!empty($rowOperation['user'])) {
                            if (!empty($rowOperation['user']['is_microservice'])) {
                                $rowOperation['user'] = trim(preg_replace('/\(api\)$/i', '', $rowOperation['user']['username']));
                            } else {
                                $rowOperation['user'] = CompanyBalanceOperation::getUsername($rowOperation['user']['name'], $rowOperation['user']['surname'], $project->name);
                            }
						}

						if (isset($rowOperation['rule_id'])) {
							$rowOperation['rule_id'] = isset($withdrawalRule[$rowOperation['rule_id']]) ? $withdrawalRule[$rowOperation['rule_id']]['id_api'] : null;
						}



						$rowOperation['inner_id'] = $rowOperation['id'];
						$rowOperation['company_id'] = $company->id;
						unset($rowOperation['id']);

						$operation = CompanyBalanceOperation::find()
						                                    ->where(['inner_id' => $rowOperation['inner_id']])
															->andWhere(['company_id' => $company->id])
															->orderBy('id')
						                                    ->one();
						if (empty($operation)) {
							$operation = new CompanyBalanceOperation();
						}
						$operation->detachBehavior('sendToProject');
						$operation->detachBehavior('setUser');
						$operation->detachBehavior('timestampBehavior');

						if ($operation->load($rowOperation, '') and $operation->save()) {
							//
						} else {
							\Yii::error([
								'message' => $operation->getFirstErrors(),
								'label' => 'Произошла ошибка при синхронизации данных (лимит) с ' . $params['site_id'] . ' компании ' . $row['name'],
							]);
						}
					}
				} else {
					\Yii::error([
						'message' => $company->getErrors(),
						'data' => $row,
						'label' => 'Произошла ошибка при синхронизации данных с ' . $params['site_id'] . ' компании ' . $row['name'],
					]);
				}
			}

		} catch (\Exception $e) {
			return BaseResponse::errorResponse($e->getMessage());
		}

		return BaseResponse::successResponse('Успешно');
	}
}
