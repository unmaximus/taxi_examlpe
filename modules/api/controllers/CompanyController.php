<?php

namespace app\modules\api\controllers;

use app\models\ar\Company;
use app\models\ar\Project;
use app\modules\system\responses\BaseResponse;
use Yii;
use yii\web\NotFoundHttpException;
use app\controllers\BaseController as AppBaseController;

class CompanyController extends AppBaseController
{
	public $modelClass = 'app\models\ar\Company';

	/**
	 * @return BaseResponse
	 */
	public function actionCreate()
	{
		$params = Yii::$app->request->post();

		$params['inner_id'] = $params['container_id'];
		$params['project_id'] = Project::getOrCreate($params['site_id'])->id;
		$params['is_synchronized'] = true;

		$model = new Company();

		$model->detachBehavior('sendToProject');

		if ($model->load($params, '') && $model->save()) {
			return BaseResponse::successResponse('Успешно');
		} else {
			return BaseResponse::errorResponse($model->getFirstErrors());
		}
	}

	/**
	 * @return BaseResponse
	 * @throws NotFoundHttpException
	 */
	public function actionUpdate()
	{
		$params = Yii::$app->request->post();

		$model = $this->findModel($params);

		if ($model->load($params, '') && $model->save()) {
			return BaseResponse::successResponse('Успешно');
		} else {
			return BaseResponse::errorResponse($model->getFirstErrors());
		}
	}


	/**
	 * @return BaseResponse
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete()
	{
		$params = Yii::$app->request->post();

		$model = $this->findModel($params);

		if ($model->delete()) {
			return BaseResponse::successResponse('Успешно');
		} else {
			return BaseResponse::errorResponse('Ошибка удаления');
		}
	}

	/**
	 * @param $params
	 *
	 * @return Company|null
	 * @throws NotFoundHttpException
	 */
	protected function findModel($params)
	{
		if (empty($params['container_id'])) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}

		$model = Company::find()->alias('c')
		                ->leftJoin(Project::tableName() . ' p', 'p.id = c.project_id')
		                ->andWhere([
			                'c.inner_id' => $params['container_id'],
			                'p.name' => $params['site_id'],
		                ])
		                ->one();
		if ($model == null) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}

		$model->detachBehavior('sendToProject');

		return $model;
	}
}
