<?php

namespace app\modules\api\controllers;

use app\modules\dashboard\models\ar\CompanyBalanceOperation;
use app\modules\dashboard\models\ar\CompanyWithdrawalRule;
use app\models\ar\Project;
use app\models\User;
use app\modules\system\responses\BaseResponse;
use Yii;
use yii\web\NotFoundHttpException;
use app\models\ar\Company;
use app\controllers\BaseController as AppBaseController;

class CompanyBalanceController extends AppBaseController
{
	public $modelClass = 'app\models\ar\CompanyBalanceOperation';

	/**
	 * @return BaseResponse
	 */
	public function actionCreateOperation()
	{
		$params = Yii::$app->request->post();

		if (empty($params['container_id'])) {
			return BaseResponse::errorResponse('Отсутсвует container_id в запросе');
		}

		$company_id = Company::find()
		                     ->select(Company::tableName() . '.id')->joinWith('project')
		                     ->where([
			                     Company::tableName() . '.inner_id' => $params['container_id'],
			                     Project::tableName() . '.name' => $params['site_id'],
		                     ])->scalar();

		if (empty($company_id)) {
			return BaseResponse::errorResponse('Отсутсвует запись о данной компании.');
		}

		$params['company_id'] = $company_id;

		if (empty($params['rule_id'])) {
			if (!empty($params['user'])) {
                $params['user'] = CompanyBalanceOperation::getUsername($params['user']['name'], $params['user']['surname'], $params['site_id']);
			}
		} else {
			$params['rule_id'] = CompanyWithdrawalRule::find()
			                               ->select('id')
			                               ->andWhere(['inner_id' => (int) $params['rule_id']])
			                               ->scalar();
			if(empty($params['rule_id'])) {
				return BaseResponse::errorResponse('Отсутсвует запись о правиле ID#' . $params['rule_id']);
			}
		}

		$model = new CompanyBalanceOperation();
		$model->detachBehavior('sendToProject');
		$model->detachBehavior('setUser');

		if ($model->load($params, '') && $model->save()) {
			return BaseResponse::successResponse('Успешно');
		} else {
			return BaseResponse::errorResponse($model->getFirstErrors());
		}
	}

	/**
	 * @param $params
	 *
	 * @return CompanyBalanceOperation|array|\yii\db\ActiveRecord|null
	 * @throws NotFoundHttpException
	 */
	protected function findModel($params)
	{
		if (empty($params['container_id']) or empty($params['inner_id'])) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}

		$model = CompanyBalanceOperation::find()->alias('clr')
		                              ->leftJoin(Company::tableName() . ' c', 'c.id = clr.company_id')
		                              ->andWhere([
			                              'c.inner_id' => $params['container_id'],
			                              'c.project_id' => Yii::$app->user->identity->company->project_id,
			                              'clr.inner_id' => $params['inner_id'],
		                              ])
		                              ->one();
		if ($model == null) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}

		$model->detachBehavior('sendToProject');

		return $model;
	}
}
