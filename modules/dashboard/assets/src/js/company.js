$(function () {
    $('#only-synchronized-filters').click(function () {
        var url = $(this).is(':checked')
            ? '?onlySynchronized=1'
            : '?onlySynchronized=0';

        $.pjax({
            url: url,
            container: '#grid-list'
        });
    });
});


function updateWithdrawalRules(data) {
    var withdrawalRule$ = $('#withdrawal-rules');

    if (withdrawalRule$) {
        withdrawalRule$.html(data);
    }
}

function updateLimitRules(data) {
    var limitRule$ = $('#limit-rules');

    if (limitRule$) {
        limitRule$.html(data);
    }
}

function extParse(data) {
    var arrData = undefined;
    if (typeof data === 'string') {
        try {
            arrData = JSON.parse(data);
        } catch (e) { return undefined; }
    } else {
        arrData = data;
    }

    return arrData;
}

function updateTableRow(data) {
    try {
        var params = extParse(data);

        if (params['table']) {
            var tableSelector = this.data('table-selector'),
                rowId = this.data('row-id');

            if (rowId) {
                var rowCols$ = $(tableSelector).find("[data-key='" + rowId + "']").children();
            } else {
                return;
            }

            $.each(params['table'], function( index, value ) {
                rowCols$.find("[data-attribute='" + index + "'] a").html(value);
            });

        }
    } catch (e) { }
}

$(document).on('show.bs.modal', '.modal', function () {
    var zIndex = 1040 + (10 * $('.modal:visible').length);
    $(this).css('z-index', zIndex);
    setTimeout(function () {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
});