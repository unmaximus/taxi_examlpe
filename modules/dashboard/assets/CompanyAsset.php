<?php

namespace app\modules\dashboard\assets;

use yii\web\AssetBundle;

/**
 * Class CompanyAsset
 * @package app\modules\dashboard\assets
 */
class CompanyAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/dashboard/assets/src';

    public $css = [
    ];

    public $js = [
	    'js/company.js',
    ];

	public $depends = [
		'yii\bootstrap\BootstrapPluginAsset',
	];
}
