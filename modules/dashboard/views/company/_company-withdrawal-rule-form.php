<?php

/* @var $this yii\web\View */

use app\modules\dashboard\models\ar\CompanyWithdrawalRule;
use app\modules\system\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $model CompanyWithdrawalRule */
/* @var $form yii\widgets\ActiveForm */

$model->begin_datetime = $model->asDatetime('begin_datetime');
$model->end_datetime = $model->asDatetime('end_datetime');

?>

<?php $form = ActiveForm::begin([
    'id' => 'company-withdrawal-form',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
//    'enableAjaxValidation' => true,
    'validateOnBlur' => false,
    'validateOnChange' => false,
    'validationUrl' => Url::to(['update', /*'ajaxMethod' => 'ajaxValidation',*/ 'model' => CompanyWithdrawalRule::className(), 'id' => $model->id]),
    'fieldConfig' => [
        'errorOptions' => [
            'encode' => false
        ],
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
]); ?>

    <div class="form-horizontal">

        <?= $form->field($model, 'name') ?>

        <?php echo $form->customContent(
	        'Статус',
            Html::checkbox('CompanyWithdrawalRule[is_active]', $model->is_active, [
                'label' => 'активна',
                'labelOptions' => [
                    'style' => 'font-weight: normal;'
                ]
            ])
        );
        ?>

        <?  echo $form->field($model, 'begin_datetime')->widget(DateTimePicker::className(),[
	        'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
	        'options' => ['placeholder' => 'Ввод даты'],
	        'convertFormat' => true,
	        'pluginOptions' => [
		        'format' => 'dd.MM.yyyy hh:i',
		        'autoclose'=>true,
		        'initialDate'=> date('Y-m-d H:00'),
		        'minView'=> 1,
		        'weekStart'=>1,
	        ]
        ]); ?>

        <?  echo $form->field($model, 'end_datetime')->widget(DateTimePicker::className(),[
	        'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
	        'options' => ['placeholder' => 'Ввод даты'],
	        'convertFormat' => true,
	        'pluginOptions' => [
		        'format' => 'dd.MM.yyyy hh:i',
		        'autoclose'=>true,
		        'initialDate'=> date('Y-m-d H:00'),
		        'minView'=> 1,
		        'weekStart'=>1,
	        ]
        ]); ?>

        <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>

        <span>&nbsp;&nbsp;<?= 'Сумма списания'?></span>
        <hr style="margin-top: 0px;">

        <?= $form->field($model, 'value') ?>

        <span>&nbsp;&nbsp;<?= 'Когда производить списание'?></span>
        <hr style="margin-top: 0px;">

        <?php echo $form->field($model, 'hour')->dropDownList(
                [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
        ) ?>

        <?= $form->field($model, 'type')->dropDownList(CompanyWithdrawalRule::typeLabels()) ?>

        <?php
        $js = "$('#companywithdrawalrule-type').off('change').on('change', function() {
            var value = $(this).val()
             
            var daily$ = $('#daily-form'), weekly$ = $('#weekly-form'), monthly$ = $('#monthly-form')
            
            daily$.hide();
            weekly$.hide();
            monthly$.hide();
            
            switch(value) {
                case '1':
                    daily$.show();
                    break;        
                case '2':
                    weekly$.show();
                    break;
                case '3':
                    monthly$.show();
                    break;
            }
        })";

        $this->registerJs($js);

        if (!isset($model->type)) {
            $model->type = CompanyWithdrawalRule::DAILY;
        }
        ?>

        <div id="daily-form" style="<?= $model->type == CompanyWithdrawalRule::DAILY ? '' : 'display: none;'?>">
            <?php echo $this->render('_rule-daily-form', ['form' => $form, 'model' => $model]); ?>
        </div>

        <div id="weekly-form" style="<?= $model->type == CompanyWithdrawalRule::WEEKLY ? '' : 'display: none;'?>">
            <?php echo $this->render('_rule-weekly-form', ['form' => $form, 'model' => $model]); ?>
        </div>

        <div id="monthly-form" style="<?= $model->type == CompanyWithdrawalRule::MONTHLY ? '' : 'display: none;'?>">
            <?php echo $this->render('_rule-monthly-form', ['form' => $form, 'model' => $model]); ?>
        </div>

    </div>

<?php ActiveForm::end(); ?>