<?php

use app\modules\dashboard\models\ar\CompanyWithdrawalRule;
use app\modules\system\widgets\AjaxModalFormButtonWidget;
use yii\helpers\Url;

/* @var $models CompanyWithdrawalRule[] */
/* @var $model CompanyWithdrawalRule */

foreach ($models as $model) {
    if (!$model->is_active) {
        $options = ['style' => ['text-decoration' => 'line-through']];
    } else {
        $options = [];
    }

    echo AjaxModalFormButtonWidget::widget([
        'id' => 'ruleModal',
        'button' => false,
        'text' => $model->ruleDescription(),
        'url' => Url::to(['update-withdrawal-rule', 'id' => $model->id]),
        'title' => $model->ruleDescription(),
        'submitButtonText' => 'Сохранить',
        'doneTrigger' => 'updateWithdrawalRules',
        'options' => $options,
    ]);

    echo '&nbsp&nbsp';

    echo \app\modules\system\widgets\AjaxActionButtonWidget::widget([
        'text' => 'x',
        'button' => false,
        'url' => Url::to(['delete-withdrawal-rule', 'id' => $model->id]),
        'afterTrigger' => 'updateWithdrawalRules',
        'options' => ['style' => ['color' => 'red']],
    ]);

    echo '<br>';
}