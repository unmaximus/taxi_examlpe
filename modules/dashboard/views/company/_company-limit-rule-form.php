<?php

/* @var $this yii\web\View */

use app\modules\dashboard\models\ar\CompanyLimitRule;
use yii\helpers\Html;
use app\modules\system\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $model CompanyLimitRule */
/* @var $form yii\widgets\ActiveForm */

?>

<?php $form = ActiveForm::begin([
    'id' => 'company-limit-form',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'validateOnBlur' => false,
    'validateOnChange' => false,
    'validationUrl' => Url::to(['update', 'ajaxMethod' => 'ajaxValidation', 'model' => CompanyLimitRule::className(), 'id' => $model->id]),
    'fieldConfig' => [
        'errorOptions' => [
            'encode' => false
        ],
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
]); ?>

    <div class="form-horizontal">

        <?php echo $form->customContent(
	        'Статус',
            Html::checkbox('CompanyLimitRule[is_active]', $model->is_active, [
                'label' => 'активно',
                'labelOptions' => [
                    'style' => 'font-weight: normal;'
                ]
            ])
        );
        ?>

        <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>

        <?php echo $form->field($model, 'type')->dropDownList(
            CompanyLimitRule::typeLabels()
        );

        ?>

        <?= $form->field($model, 'limit') ?>

        <?php echo Html::activeHiddenInput($model, 'company_id');
        if (!$model->isNewRecord) {
            echo Html::activeHiddenInput($model, 'id');
        }
        ?>

    </div>

<?php ActiveForm::end(); ?>