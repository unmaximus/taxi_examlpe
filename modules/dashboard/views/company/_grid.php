<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\system\grid\ActionColumn;
use app\modules\system\widgets\AjaxModalFormButtonWidget;
use \yii\helpers\Url;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\search\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<?php Pjax::begin(['options' => ['data-pjax-container' => 'grid-list']]); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    'filterUrl' => 'index',
    'id' => 'grid-list',
    'columns' => [
        [
            'attribute' => 'inner_id',
            'label' => 'внутренний ID ',
            'filterOptions' => ['style' => 'width: 150px']
        ],
        [
            'attribute' => 'domain',
            'label' => 'Домены',
            'format' => 'raw',
            'value' => function ($model) {
                $url = 'http://' . rtrim(trim($model['domain']), '*') . '/companies/company/index';

                return Html::a($model['domain'], $url, ['target' => '_blank']);
            },
        ],
        'company:text:Комапании',
        [
            'attribute' => 'balance',
            'label' => 'Баланс',
            'value' => function ($model) {
                return Yii::$app->formatter->asDecimal($model['balance'] ?? '', 2);
            },
        ],
        [
            'attribute' => 'limit_rules',
            'label' => 'Ограничения',
            'value' => function ($model) {
                $labels = \app\modules\dashboard\models\ar\CompanyLimitRule::typeLabels();
                $rules = explode(',', $model['limit_rules']);
                $result = '';
                foreach ($rules as $rule) {
                    if(empty($rule)) {
                        continue;
                    }
                    list($type, $limit) = explode('-', $rule);
                    $result .= isset($labels[$type]) ? ', ' . $labels[$type] . ' - ' . $limit : '';
                }
                return trim($result, ', ');
            },
        ],
        'withdrawal_rules:text:Правила списания',
        [
            'attribute' => 'is_active',
            'label' => 'Статус',
            'value' => function ($model) {
                return $model['is_active'] ? 'активна' : 'не активна';
            },
            'filter' => [0 => 'не активна', 1 => 'активна'],
        ],
        [
            'attribute' => 'date_of_blocking',
            'label' => 'Дата блокировки',
            'value' => function ($model) {
                return $model['date_of_blocking'];
            },
        ],
        [
            'class' => ActionColumn::className(),
            'headerOptions' => ['style' => 'width:60px;'],
            'template' => '{create-operation}{update}{history}',
	        'urlCreator' => function ($action, $model, $key, $index) {
		        $params = is_array($key) ? $key : ['id' => (string)$model['id']];
		        $params[0] = Yii::$app->controller->id ? Yii::$app->controller->id . '/' . $action : $action;

		        $queryParams = Yii::$app->request->getQueryParams();
		        unset($queryParams['id']);

		        $params = \app\modules\system\helpers\ArrayHelper::merge($params, $queryParams);

		        return \yii\helpers\Url::toRoute($params);
            },
	        'buttons' => [
                'update' => function ($url, $model) {
                    /** @var \app\models\ar\Company $model */
                    return AjaxModalFormButtonWidget::widget([
                        'button' => false,
                        'text' => '<span class="glyphicon glyphicon-pencil"></span>',
                        'modalSize' => AjaxModalFormButtonWidget::LARGE,
                        'url' => $url,
                        'title' => $model['company'],
                        'submitButtonText' => 'Сохранить',
                        'resultContainer' => '#company-grid',
                    ]);
                },
                'create-operation' => function ($url, $model) {
                    /** @var \app\models\ar\Company $model */
                    return AjaxModalFormButtonWidget::widget([
                        'id' => 'balanceModal',
                        'button' => false,
                        'text' => '<span class="glyphicon glyphicon-plus"></span>',
                        'url' => $url,
                        'title' => 'Баланс компании' . ' ' .  $model['company'],
                        'submitButtonText' => 'Добавить',
                        'resultContainer' => '#company-grid',
                        'options' => [
                            'data-table-selector' => '#company-grid-widget',
                            'data-row-id' => $model['id'],
                        ]
                    ]);
                },
                'history' => function($url, $model) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-list"></span>',
                        ['/dashboard/balance/index', 'id' => $model['id']],
                        ['target' => '_blank', 'data-pjax' => 0]);
                }
            ],
        ],
    ],
]); ?>

<?php Pjax::end(); ?>




