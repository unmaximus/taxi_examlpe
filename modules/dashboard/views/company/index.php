<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\search\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

\app\modules\dashboard\assets\CompanyAsset::register($this);

$this->title = 'Компании';
$this->params['breadcrumbs'][] = $this->title;

echo $this->render('/monitoring/_menu');

?>
<div class="container-fluid">

    <h1><?= Html::encode($this->title) ?></h1>

    <div id="company-grid">
	    <?= $this->render('_grid', [
		    'dataProvider' => $dataProvider,
		    'searchModel' => $searchModel,
	    ]);
	    ?>
    </div>

    <form class="form-inline">
        <div class="row">
            <div class="col-md-2">
                <p class=""><b>*</b> - синхронизированные компании</p>
            </div>
            <div class="col-md-2 col-md-offset-8 checkbox">
				<?= Html::checkbox('only-synchronized', $searchModel->onlySynchronized, ['label' => 'Только синхронизированные компании', 'id' => 'only-synchronized-filters']) ?>
            </div>
        </div>
    </form>

</div>
