<?php

use app\modules\dashboard\models\ar\CompanyLimitRule;
use app\modules\system\widgets\AjaxModalFormButtonWidget;
use yii\helpers\Url;

/* @var $models CompanyLimitRule[] */
/* @var $model CompanyLimitRule */

foreach ($models as $model) {
    if (!$model->is_active) {
        $options = ['style' => ['text-decoration' => 'line-through']];
    } else {
        $options = [];
    }

    echo AjaxModalFormButtonWidget::widget([
        'id' => 'ruleModal',
        'button' => false,
        'text' => $model->ruleDescription(),
        'url' => Url::to(['update-limit-rule', 'id' => $model->id]),
        'title' => $model->ruleDescription(),
        'submitButtonText' => 'Сохранить',
        'doneTrigger' => 'updateLimitRules',
        'options' => $options,
    ]);

    echo '&nbsp&nbsp';

    echo \app\modules\system\widgets\AjaxActionButtonWidget::widget([
        'text' => 'x',
        'button' => false,
        'url' => Url::to(['delete-limit-rule', 'id' => $model->id]),
        'afterTrigger' => 'updateLimitRules',
        'options' => ['style' => ['color' => 'red']],
    ]);

    echo '<br>';
}