<?php

use app\modules\dashboard\models\ar\CompanyWithdrawalRule;
use kartik\select2\Select2;

/* @var $model CompanyWithdrawalRule */
/* @var $form \app\modules\system\widgets\ActiveForm */

echo $form->field($model, 'weekly_every');

echo $form->field($model, 'weekly_days')->widget(Select2::className(), [
        'options' => [
            'multiple' => true,
            'theme' => Select2::THEME_BOOTSTRAP,
            'placeholder' => 'дни недели',
        ],
        'data' =>  \app\modules\system\helpers\EnumerateHelper::dayList(),
]);
