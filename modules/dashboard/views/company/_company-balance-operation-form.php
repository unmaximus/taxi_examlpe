<?php

/* @var $this yii\web\View */

use app\modules\dashboard\models\ar\CompanyBalanceOperation;
use app\modules\system\widgets\ActiveForm;
use yii\helpers\Url;


/* @var $model CompanyBalanceOperation */
/* @var $form yii\widgets\ActiveForm */

?>

<?php $form = ActiveForm::begin([
    'id' => 'company-balance-operation-form',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    /*'enableAjaxValidation' => true,*/
    'validateOnBlur' => false,
    'validateOnChange' => false,
    'validationUrl' => Url::to(['update', 'ajaxMethod' => 'ajaxValidation', 'id' => $model->id]),
    'fieldConfig' => [
        'errorOptions' => [
            'encode' => false
        ],
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
]); ?>

    <div class="form-horizontal">

        <?php
        $types = CompanyBalanceOperation::typeLabels();
        unset($types[CompanyBalanceOperation::TYPE_RULE_OUT]);
        echo $form->field($model, 'type')->dropDownList($types) ?>

        <?php echo  $form->field($model, 'value') ?>

        <?php echo $form->field($model, 'comment')->textarea(['rows' => 4]) ?>

    </div>

<?php ActiveForm::end(); ?>