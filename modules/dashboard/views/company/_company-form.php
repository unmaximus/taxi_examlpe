<?php

/* @var $this yii\web\View */

use app\modules\system\widgets\ActiveForm;
use app\modules\system\widgets\AjaxModalFormButtonWidget;
use kartik\datetime\DateTimePicker;
use yii\helpers\Url;

/* @var $model \app\models\ar\Company */
/* @var $form yii\widgets\ActiveForm */


$model->date_of_blocking = $model->asDatetime('date_of_blocking');

?>

<?php $form = ActiveForm::begin([
    'id' => 'company-form',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
//    'enableAjaxValidation' => true,
    'validateOnBlur' => false,
    'validateOnChange' => false,
    'validationUrl' => Url::to(['update', /*'ajaxMethod' => 'ajaxValidation',*/ 'id' => $model->id]),
    'fieldConfig' => [
        'errorOptions' => [
            'encode' => false
        ],
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
]); ?>

    <div class="form-horizontal">

        <?php echo $form->field($model, 'name') ?>
        <?php echo $form->field($model, 'is_active')->checkbox()->label('активна',['style' => 'font-weight: normal']) ?>
	    <?php echo $form->customContent('Баланс', $model->balance) ?>


        <?php
            $content = '<div id="withdrawal-rules">' ;

            $content .= $this->renderAjax('_withdrawal-rule-description', [
                'models' => $model->companyWithdrawalRule
            ]);

            $content .= '</div>';

            $content .= AjaxModalFormButtonWidget::widget([
                'id' => 'additionalModal',
                'button' => false,
                'text' => 'Добавить новое правило',
                'url' => Url::to(['create-withdrawal-rule', 'id' => $model->id]),
                'title' => 'Новое правило списаний',
                'submitButtonText' => 'Создать',
                'doneTrigger' => 'updateWithdrawalRules',
            ]);

            echo $form->customContent(
               'Правила списания',
                $content
            ) ?>

        <hr>

        <?php
        $content = '<div id="limit-rules">' ;

        $content .= $this->renderAjax('_limit-rule-description', [
            'models' => $model->companyLimitRule
        ]);

        $content .= '</div>';

        $content .= AjaxModalFormButtonWidget::widget([
            'id' => 'additionalModal',
            'button' => false,
            'text' => 'Добавить новое ограничение',
            'url' => Url::to(['create-limit-rule', 'id' => $model->id]),
            'title' => 'Новое ограничение',
            'submitButtonText' => 'Создать',
            'doneTrigger' => 'updateLimitRules',
        ]);

        echo $form->customContent(
            'Ограничения',
            $content
        ) ?>

        <hr>

        <?php echo $form->field($model, 'date_of_blocking')->widget(DateTimePicker::className(),[
	        'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
	        'options' => ['placeholder' => 'Ввод даты'],
	        'convertFormat' => true,
	        'pluginOptions' => [
		        'format' => 'dd.MM.yyyy hh:i',
		        'autoclose'=>true,
		        'initialDate'=> date('Y-m-d H:00'),
		        'minView'=> 1,
		        'weekStart'=>1,
	        ]
        ]); ?>

        <hr>

        <?= $form->customContent(
	        $model->getAttributeLabel('created_at'),
	        $model->created_at ? $model->asDatetime('created_at') : '-'
        ) ?>

        <?= $form->customContent(
	        $model->getAttributeLabel('created_by'),
            isset($model->user) ? $model->user->username : '-'
        ) ?>

        <hr>

        <?= $form->field($model, 'comment')->textarea(['rows' => 4]) ?>

    </div>

<?php ActiveForm::end(); ?>