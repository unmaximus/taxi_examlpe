<?php

use app\modules\dashboard\DashboardModule;
use app\modules\dashboard\models\search\ProjectSettingSearch;
use app\modules\system\grid\BooleanColumn;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\widgets\Pjax;


/**
 * @var $this         yii\web\View
 * @var $model        ProjectSettingSearch
 * @var $dataProvider ActiveDataProvider
 */


$this->title = DashboardModule::t('Мониторинг доменов');

echo $this->render('_menu');
?>
<h1><?= $this->title ?></h1>


<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns'      => [
        'project.name',
        [
            'class'          => BooleanColumn::className(),
            'attribute'      => 'isTestMailerWeb',
            'format'         => 'boolean',
        ],
        [
            'class'          => BooleanColumn::className(),
            'attribute'      => 'isTestMailerConsole',
            'format'         => 'boolean',
        ],
        [
            'class'          => BooleanColumn::className(),
            'attribute'      => 'isNotification',
            'format'         => 'boolean',
        ],
        [
            'class'          => BooleanColumn::className(),
            'attribute'      => 'isRemind',
            'format'         => 'boolean',
        ],
        [
            'class'          => BooleanColumn::className(),
            'attribute'      => 'isScheduler',
            'format'         => 'boolean',
        ],
        [
            'class'          => BooleanColumn::className(),
            'attribute'      => 'isFinePull',
            'format'         => 'boolean',
        ],
        [
            'class'          => BooleanColumn::className(),
            'attribute'      => 'isFinePush',
            'format'         => 'boolean',
        ],
        [
            'class'          => BooleanColumn::className(),
            'attribute'      => 'isFineCheck',
            'format'         => 'boolean',
        ],
        [
            'class'          => BooleanColumn::className(),
            'attribute'      => 'isSap',
            'format'         => 'boolean',
        ],
        [
            'class'          => BooleanColumn::className(),
            'attribute'      => 'isBoss',
            'format'         => 'boolean',
        ],
    ],
]); ?>


