<?php

use app\modules\dashboard\DashboardModule;
use app\modules\system\grid\BooleanColumn;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

/**
 * @var $this         yii\web\View
 * @var $model        \app\modules\dashboard\models\ar\FineMonitoring
 * @var $dataProvider ArrayDataProvider
 */


$this->title = DashboardModule::t('ТС');

?>
<div class="collapse multi-collapse" id="<?= $model->getInfoId() ?>">
    <h1><?= $this->title ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'sts',
            'number',
            'message',
        ],
    ]); ?>
</div>


