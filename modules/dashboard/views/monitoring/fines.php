<?php

use app\modules\dashboard\DashboardModule;
use app\modules\dashboard\models\search\ProjectSettingSearch;
use app\modules\system\grid\BooleanColumn;
use yii\data\ActiveDataProvider;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;


/**
 * @var $this         yii\web\View
 * @var $model        \app\modules\dashboard\models\search\FineMonitoringSearch
 * @var $dataProvider ActiveDataProvider
 */
$view = $this;
$this->title = DashboardModule::t('Штрафы');
\yii\bootstrap\BootstrapPluginAsset::register($this);
echo $this->render('_menu');
?>
    <h1><?= $this->title ?></h1>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'monitoringTypeLabel',
            'format' => 'raw',
            'value' => function ($model) use (&$view) {
                /**
                 * @var $model \app\modules\dashboard\models\ar\FineMonitoring
                 */
                $label = $model->monitoringTypeLabel;
                if ($model->extraInfoCallback) {
                    $fc = $model->extraInfoCallback;
                    /**
                     * @var $dataProvider \yii\data\ArrayDataProvider
                     */
                    $dataProvider = $model->{$fc};
                    $dataProvider->pagination->pageSize = 0;

                    $label .= $view->render('cars', [
                        'dataProvider' => $dataProvider,
                        'model' => $model,
                    ], $view);
                }

                return $label;
            }
        ],
        [
            'class' => DataColumn::className(),
            'attribute' => 'monitoringValue',
            'contentOptions' => function ($model) {
                if ($model->isWarning()) {
                    return ['class' => 'danger'];
                }

                return ['class' => 'success'];
            }
        ],
    ],
]); ?>