<?php

use yii\bootstrap\Nav;

/**
 * @var $this         yii\web\View
 */


echo Nav::widget([
    'items' => [
        [
            'label' => 'Конфигурации доменов',
            'url' => ['monitoring/index'],
        ],
        ['label' => 'Штрафы',
            'url' => ['monitoring/fines'],
        ],
        ['label' => 'Мониторинг штрафов',
            'url' => ['fine-settings/index'],
        ],
        [
            'label' => 'Логи',
            'url' => ['logs/index'],
        ],
        [
            'label' => 'Сервисы штрафов у компаний',
            'url' => ['fine-company-service/index'],
        ],
        [
            'label' => 'Прокси',
            'url' => ['proxy/index'],
        ],
        [
            'label' => 'Аккаунты автокода',
            'url' => ['avtokod-account/index'],
        ],
        [
            'label' => 'Сервисы штрафов',
            'url' => ['fine-service/index'],
        ],
        [
            'label' => 'Парсинг лицензий такси',
            'url' => ['parser/taxi-license'],
        ],
        [
            'label' => 'Управление компаниями',
            'url' => ['company/index'],
        ],
        [
            'label' => 'Выйти',
            'url' => ['/site/logout'],
        ],
    ],
    'options' => ['class' => 'nav-pills'],
]);