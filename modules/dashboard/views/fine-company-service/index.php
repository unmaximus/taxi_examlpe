<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \app\models\search\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сервисы штрафов компаний';
$this->params['breadcrumbs'][] = $this->title;
echo $this->render('/monitoring/_menu');

?>
<div class="avtokod-account-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php echo  \yii\bootstrap\Alert::widget()?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'name',
            [
                'attribute' => 'project_id',
                'label' => 'Проект',
                'value' => function ($model) {

                    return \app\modules\system\helpers\ArrayHelper::getValue($model, 'project.name');
                },
                'filter' => \app\models\search\ProjectSearch::getList()
            ],
            //'status:boolean',
            'inner_id',
            [
                'attribute' => 'service_id',
                'label' => 'Сервисы',
                'filter' => \app\modules\fine\models\search\ServiceSearch::getList(),
                'value' => function ($model) {

                    $names = \app\modules\system\helpers\ArrayHelper::getColumn($model->services, 'name');

                    return implode(',', $names);

                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
            ],
        ],
    ]); ?>
</div>
