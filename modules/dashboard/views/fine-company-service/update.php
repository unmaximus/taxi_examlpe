<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\modules\dashboard\models\forms\FineCompanyServiceForm */

$this->title = 'Компания:' . $model->name . ' ' . $model->project->name;

?>
<div class="update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
