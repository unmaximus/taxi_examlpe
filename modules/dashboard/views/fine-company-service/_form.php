<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\modules\dashboard\models\forms\FineCompanyServiceForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="avtokod-account-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    foreach (\app\modules\fine\models\ar\Service::getServices() as $service)
    {
        Yii::error($model->getAttribute($service->type));
        echo $form->field($model, $service->type)->checkbox();

    }
    ?>




    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
