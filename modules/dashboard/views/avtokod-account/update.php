<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\fine\models\ar\AvtokodAccount */

$this->title = 'Update Avtokod Account: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Avtokod Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="avtokod-account-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
