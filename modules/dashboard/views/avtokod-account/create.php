<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\fine\models\ar\AvtokodAccount */

$this->title = 'Create Avtokod Account';
$this->params['breadcrumbs'][] = ['label' => 'Avtokod Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="avtokod-account-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
