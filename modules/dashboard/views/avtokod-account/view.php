<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\fine\models\ar\AvtokodAccount */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Avtokod Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="avtokod-account-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'password',
            'proxy_id',
            'fingerprint',
            'hold_till',
            'last_warning_at',
            'last_block_at',
            'last_usage_at',
            'warning_count',
            'is_blocked:boolean',
            'is_active:boolean',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
