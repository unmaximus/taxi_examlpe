<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\fine\models\ar\AvtokodAccount */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="avtokod-account-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fingerprint')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hold_till')->textInput() ?>

    <?= $form->field($model, 'last_warning_at')->textInput() ?>

    <?= $form->field($model, 'last_block_at')->textInput() ?>

    <?= $form->field($model, 'last_usage_at')->textInput() ?>

    <?= $form->field($model, 'warning_count')->textInput() ?>

    <?= $form->field($model, 'is_blocked')->checkbox() ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
