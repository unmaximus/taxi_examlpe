<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\fine\models\search\AvtokodAccountSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="avtokod-account-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'password') ?>

    <?= $form->field($model, 'proxy_id') ?>

    <?= $form->field($model, 'fingerprint') ?>

    <?php // echo $form->field($model, 'hold_till') ?>

    <?php // echo $form->field($model, 'last_warning_at') ?>

    <?php // echo $form->field($model, 'last_block_at') ?>

    <?php // echo $form->field($model, 'last_usage_at') ?>

    <?php // echo $form->field($model, 'warning_count') ?>

    <?php // echo $form->field($model, 'is_blocked')->checkbox() ?>

    <?php // echo $form->field($model, 'is_active')->checkbox() ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
