<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\fine\models\search\AvtokodAccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Avtokod Accounts';
$this->params['breadcrumbs'][] = $this->title;
echo $this->render('/monitoring/_menu');
?>
<div class="avtokod-account-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Avtokod Account', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'password',
            'proxy_id',
            'fingerprint',
            'hold_till',
            'last_warning_at',
            'last_block_at',
            'last_usage_at',
            'warning_count',
            'is_blocked:boolean',
            'is_active:boolean',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
