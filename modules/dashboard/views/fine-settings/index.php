<?php

use app\modules\dashboard\models\ar\FineMonitoring;
use app\modules\system\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\search\FineMonitoringSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мониторинг штрафов';
$this->params['breadcrumbs'][] = $this->title;


echo $this->render('/monitoring/_menu');
?>


<div class="fine-monitoring-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать новый мониторинг', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'project.name',
            'fine_type',
            [
                'attribute' => 'monitoring_type',
                'value' => function ($model) {
                    return ArrayHelper::getValue(FineMonitoring::getMonitoringTypeLabels(), $model->monitoring_type);
                },
            ],
            'is_fine_service:boolean',
            'value',
            'threshold',
            // 'sort',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
            ],
        ],
    ]); ?>
</div>
