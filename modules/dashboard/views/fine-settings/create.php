<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\dashboard\models\ar\FineMonitoring */

$this->title = 'Добавить мониторинг';
$this->params['breadcrumbs'][] = ['label' => 'Мониторинг штрафов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


echo $this->render('/monitoring/_menu');
?>
<div class="fine-monitoring-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
