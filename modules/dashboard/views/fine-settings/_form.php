<?php

use app\models\search\ProjectSearch;
use app\modules\dashboard\models\ar\FineMonitoring;
use app\modules\system\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\dashboard\models\ar\FineMonitoring */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="fine-monitoring-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'project_id')->dropDownList(ProjectSearch::getList()) ?>

    <?= $form->field($model, 'fine_type')->dropDownList(FineMonitoring::getFineTypes(), ['prompt' => 'Все']) ?>

    <?= $form->field($model, 'monitoring_type')->dropDownList(FineMonitoring::getMonitoringTypeLabels(), ['prompt' => 'Выбрать']) ?>

    <?= $form->field($model, 'is_fine_service')->checkbox() ?>

    <?= $form->field($model, 'sort')->textInput() ?>

    <p>
        Поле <b>значение</b> используется для следующих типов мониторинга
    <ul>
        <li>
            <?= ArrayHelper::getValue(FineMonitoring::getMonitoringTypeLabels(), FineMonitoring::MONITORING_TYPE_NOT_UPDATED_COUNT)?> -
            <b>значение</b> равно количеству дней X после которых считается количество не провереных ТС
        </li>
        <li>
            <?= ArrayHelper::getValue(FineMonitoring::getMonitoringTypeLabels(), FineMonitoring::MONITORING_TYPE_UPDATED_COUNT)?> -
            <b>значение</b> равно количеству последних дней X за которые считаются количество провереных ТС
        </li>
        <li>
            <?= ArrayHelper::getValue(FineMonitoring::getMonitoringTypeLabels(), FineMonitoring::MONITORING_TYPE_FINES_COUNT)?> -
            <b>значение</b> равно количеству дней X за которые считается количество полученых штрафов
        </li>
    </ul>
    </p>
    <?= $form->field($model, 'value')->textInput() ?>

    <p>
        Поле <b>порог</b> служит для настройки уведомлений и визуального отображение статустов мониторинга
        Поле <b>порог</b> используется для следующих типов мониторинга
    <ul>
        <li>
            <?= ArrayHelper::getValue(FineMonitoring::getMonitoringTypeLabels(), FineMonitoring::MONITORING_TYPE_LAST_FINE)?> -
            <b>порог</b> равно количеству дней с момента последнего штрафа, если показатель вырастит выше этого порога будет создано уведомление
            , значение по умолчанию <b><?= ArrayHelper::getValue(FineMonitoring::getDefaultThresholds(), FineMonitoring::MONITORING_TYPE_LAST_FINE)?></b>.
        </li>
        <li>
            <?= ArrayHelper::getValue(FineMonitoring::getMonitoringTypeLabels(), FineMonitoring::MONITORING_TYPE_NOT_UPDATED_COUNT)?> -
            <b>порог</b> равно количеству не проверенных ТС, если показатель вырастит выше этого порога будет создано уведомление
            , значение по умолчанию <b><?= ArrayHelper::getValue(FineMonitoring::getDefaultThresholds(), FineMonitoring::MONITORING_TYPE_NOT_UPDATED_COUNT)?></b>.
        </li>
        <li>
            <?= ArrayHelper::getValue(FineMonitoring::getMonitoringTypeLabels(), FineMonitoring::MONITORING_TYPE_UPDATED_COUNT)?> -
            <b>порог</b> равно количеству проверенных ТС, если показатель упадет ниже этого порога будет создано уведомление
            , значение по умолчанию <b><?= ArrayHelper::getValue(FineMonitoring::getDefaultThresholds(), FineMonitoring::MONITORING_TYPE_UPDATED_COUNT)?></b>.
        </li>
        <li>
            <?= ArrayHelper::getValue(FineMonitoring::getMonitoringTypeLabels(), FineMonitoring::MONITORING_TYPE_FINES_COUNT)?> -
            <b>порог</b> равно количеству полученых штрафов, если показатель упадет ниже этого порога будет создано уведомление
            , значение по умолчанию <b><?= ArrayHelper::getValue(FineMonitoring::getDefaultThresholds(), FineMonitoring::MONITORING_TYPE_FINES_COUNT)?></b>.
        </li>
    </ul>
    </p>

    <?= $form->field($model, 'threshold')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
