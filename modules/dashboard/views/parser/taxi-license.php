<?php


use app\modules\taxi\components\parsers\TaxiLicenseParser;
use yii\bootstrap\Alert;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

echo $this->render('/monitoring/_menu');
?>


<?php
if (Yii::$app->request->isPost) {
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Задача поставлена в очередь'
    ]);
}
ActiveForm::begin();

echo Html::submitButton('Загрузить', ['class' => 'btn btn-success']);

ActiveForm::end();


echo \yii\grid\GridView::widget([
    'dataProvider' => TaxiLicenseParser::getDataProvider(),
    'columns' => [
        'name',
        [
            'class' => \yii\grid\ActionColumn::className(),
            'template' => '{download}',
            'buttons' => [
                'download' => function ($url, $model) {

                    return Html::a($model['name'], ['download', 'name' => $model['name']]);
                }
            ],
            'header' => 'Скачать',
        ]
    ],
])
?>



