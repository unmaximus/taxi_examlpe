<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\search\BalanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\models\ar\Company */

$this->title = 'История операций по балансу компании ' . $model->name;
$this->params['breadcrumbs'][] = 'Компании';

echo $this->render('/monitoring/_menu');
?>
<div class="container-fluid company-balance-operation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
	    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
	        [
		        'attribute' => 'inner_id',
		        'label' => 'внутренний ID ',
	        ],
	        [
		        'attribute' => 'created_at',
		        'value' => function($model) {
			        return $model->asDatetimeTz('created_at');
		        }
	        ],
	        [
		        'attribute' => 'user',
		        'label' => 'Создана',
		        'value' => function ($model) {
                    if (isset($model->user)) {
                        $user = $model->user;
                    } elseif (isset($model->companyWithdrawalRule)) {
                        $user = $model->companyWithdrawalRule->ruleDescription();
                    } else {
                        $user = '';
                    }

			        return $user;
		        },
	        ],
	        [
		        'attribute' => 'type',
		        'label' => 'Тип',
		        'value' => function ($model) {
                    $types = \app\modules\dashboard\models\ar\CompanyBalanceOperation::typeLabels();
			        return isset($types[$model->type]) ? $types[$model->type] : $model->type;
		        },
	        ],
	        [
		        'attribute' => 'value',
		        'value' => function ($model) {
			        return Yii::$app->formatter->asDecimal($model->value ?? '', 2);
		        },
	        ],
            'comment:ntext',
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
