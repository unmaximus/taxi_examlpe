<?php

namespace app\modules\dashboard\models\search;

use app\modules\dashboard\models\ar\CompanyLimitRule;
use app\models\ar\Project;
use app\modules\dashboard\models\ar\CompanyWithdrawalRule;
use app\modules\system\components\UserDateFormat;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ar\Company as CompanyModel;
use yii\db\Expression;
use yii\db\Query;
use Yii;

/**
 * Company represents the model behind the search form of `app\models\ar\Company`.
 */
class CompanySearch extends Model
{
	public $id;
	public $name;
	public $project_id;
	public $status;
	public $inner_id;
	public $balance;
	public $comment;
	public $date_of_blocking;
	public $is_active;
	public $created_at;
	public $updated_at;
	public $created_by;
	public $updated_by;
	public $limit_rules;
	public $withdrawal_rules;
	public $domain;
	public $company;
	public $onlySynchronized = true;

	/**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company', 'domain', 'date_of_blocking', 'limit_rules', 'withdrawal_rules'], 'string'],
            [['is_active', 'onlySynchronized'], 'boolean'],
            [['balance'], 'filter', 'filter' => function ($value) {
	            if (!empty($value) && is_string($value)) {
		            $value = str_replace([','], "", $value);
		            $value = (float) $value;
	            }

	            return $value;
            }],
            [['inner_id'], 'integer'],
            [['balance'], 'double'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
	    $subQuery = (new Query())
		    ->select([
			    'c.id',
			    'c.inner_id',
			    'company' => 'c.name',
			    'c.project_id',
			    'date_of_blocking' => new Expression("
			            CASE
			            WHEN c.date_of_blocking < c.predictable_date_of_blocking OR c.predictable_date_of_blocking IS NULL THEN TO_CHAR(c.date_of_blocking, 'DD.MM.YYYY HH24:MI') || ' (установлена)'
			            WHEN c.predictable_date_of_blocking IS NOT NULL THEN TO_CHAR(c.predictable_date_of_blocking, 'DD.MM.YYYY HH24:MI') || ' (прогноз)'
			            ELSE NULL
			            END"),
			    'date_of_blocking_sort' => new Expression("
			            CASE
			            WHEN c.date_of_blocking < c.predictable_date_of_blocking OR c.predictable_date_of_blocking IS NULL THEN c.date_of_blocking 
			            WHEN c.predictable_date_of_blocking IS NOT NULL THEN c.predictable_date_of_blocking
			            ELSE NULL
			            END"),
			    'c.is_active',
			    'c.is_synchronized',
			    'c.balance',
			    'domain' => new Expression("
			            CASE is_synchronized
			            WHEN TRUE THEN p.name || '*'
			            ELSE p.name
			            END"),
			    'limit_rules' => new Expression("ARRAY_TO_STRING(ARRAY_AGG(DISTINCT CONCAT_WS('-', l.type, l.limit)), ',')"),
			    'withdrawal_rules' => new Expression("ARRAY_TO_STRING(ARRAY_AGG(DISTINCT w.name::VARCHAR), ', ')"),
		    ])
		    ->from(['c' => CompanyModel::tableName()])
		    ->leftJoin(Project::tableName() . ' p', 'c.project_id = p.id')
		    ->leftJoin(CompanyLimitRule::tableName() . ' l', 'c.id = l.company_id AND l.is_active = TRUE')
		    ->leftJoin(CompanyWithdrawalRule::tableName() . ' w', 'c.id = w.company_id AND w.is_active = TRUE')
		    ->groupBy([
			    'c.id',
			    'c.inner_id',
			    'c.name',
			    'c.project_id',
			    'c.predictable_date_of_blocking',
			    'c.date_of_blocking',
			    'c.is_active',
			    'c.is_synchronized',
			    'c.balance',
			    'p.name',
		    ]);

	    if (isset($params['onlySynchronized'])) {
		    Yii::$app->session->set('onlySynchronizedCompany', empty($params['onlySynchronized']) ? false : true);
	    }
	    if (Yii::$app->session->has('onlySynchronizedCompany')) {
		    $this->onlySynchronized = Yii::$app->session->get('onlySynchronizedCompany');
	    }
	    if ($this->onlySynchronized) {
		    $subQuery->andFilterWhere(['c.is_synchronized' => $this->onlySynchronized]);
	    }

	    $query = (new Query())->select(['*'])->from(['companies' => $subQuery]);

	    $dataProvider = new ActiveDataProvider([
		    'query' => $query,
		    'pagination' => [
			    'defaultPageSize' => 20,
		    ],
		    'sort' => [
			    'attributes' => [
				    'company' => [
					    'asc' => ['company' => SORT_ASC],
					    'desc' => ['company' => SORT_DESC],
				    ],
				    'domain' => [
					    'asc' => ['domain' => SORT_ASC],
					    'desc' => ['domain' => SORT_DESC],
				    ],
				    'domain_company' => [
					    'asc' => ['company' => SORT_ASC, 'domain' => SORT_ASC],
					    'desc' => ['company' => SORT_DESC, 'domain' => SORT_DESC],
				    ],
				    'date_of_blocking' => [
					    'asc' => ['date_of_blocking_sort' => SORT_ASC],
					    'desc' => ['date_of_blocking_sort' => SORT_DESC],
				    ],
				    'limit_rules' => [
					    'asc' => ['limit_rules' => SORT_ASC],
					    'desc' => ['limit_rules' => SORT_DESC],
				    ],
				    'withdrawal_rules' => [
					    'asc' => ['withdrawal_rules' => SORT_ASC],
					    'desc' => ['withdrawal_rules' => SORT_DESC],
				    ],
				    'balance' => [
					    'asc' => ['balance' => SORT_ASC],
					    'desc' => ['balance' => SORT_DESC],
				    ],
				    'is_active' => [
					    'asc' => ['is_active' => SORT_ASC],
					    'desc' => ['is_active' => SORT_DESC],
				    ],
				    'inner_id' => [
					    'asc' => ['inner_id' => SORT_ASC],
					    'desc' => ['inner_id' => SORT_DESC],
				    ],
			    ],
			    'defaultOrder' => ['domain' => SORT_ASC, 'inner_id' => SORT_DESC],
		    ],
	    ]);

        $this->load($params);

        if (!$this->validate()) {
            // $query->where('0=1');
            return $dataProvider;
        }

	    $query->andFilterWhere(['is_active' => $this->is_active,]);
	    $query->andFilterWhere(['balance' => $this->balance,]);

        $query->andFilterWhere(['ilike', 'domain', $this->domain])
              ->andFilterWhere(['ilike', 'company', $this->company])
              ->andFilterWhere(['ilike', new Expression('"date_of_blocking"::text'), $this->date_of_blocking])
              ->andFilterWhere(['ilike', 'limit_rules', $this->limit_rules])
              ->andFilterWhere(['ilike', 'withdrawal_rules', $this->withdrawal_rules])
	          ->andFilterWhere(['ilike',  new Expression('"inner_id"::text'), $this->inner_id]);

        return $dataProvider;
    }
}
