<?php

namespace app\modules\dashboard\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\dashboard\models\ar\CompanyBalanceOperation;

/**
 * BalanceSearch represents the model behind the search form of `app\models\ar\CompanyBalanceOperation`.
 */
class BalanceSearch extends CompanyBalanceOperation
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'inner_id', 'rule_id', 'type',], 'integer'],
            [['value', 'prev_balance'], 'number'],
            [['comment', 'created_at'], 'safe'],
            [['user',], 'string'],
            [['is_deleted'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyBalanceOperation::find()->with(['company.project']);

        if (isset($params['id'])) {
        	$query->andWhere(['company_id' => $params['id']]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
	        'sort' => [
		        'attributes' => [
			        'inner_id' => [
				        'asc' => ['inner_id' => SORT_ASC],
				        'desc' => ['inner_id' => SORT_DESC],
			        ],
			        'created_at' => [
				        'asc' => ['created_at' => SORT_ASC],
				        'desc' => ['created_at' => SORT_DESC],
			        ],
			        'user' => [
				        'asc' => ['user' => SORT_ASC],
				        'desc' => ['user' => SORT_DESC],
			        ],
			        'type' => [
				        'asc' => ['type' => SORT_ASC],
				        'desc' => ['type' => SORT_DESC],
			        ],
			        'value' => [
				        'asc' => ['value' => SORT_ASC],
				        'desc' => ['value' => SORT_DESC],
			        ],
			        'comment' => [
				        'asc' => ['comment' => SORT_ASC],
				        'desc' => ['comment' => SORT_DESC],
			        ],
		        ],
		        'defaultOrder' => ['created_at' => SORT_DESC],
	        ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'rule_id' => $this->rule_id,
            'type' => $this->type,
            'value' => $this->value,
            'created_at' => $this->created_at,
            'user' => $this->user,
            'is_deleted' => $this->is_deleted,
            'prev_balance' => $this->prev_balance,
        ]);

        $query->andFilterWhere(['ilike', 'comment', $this->comment]);

        return $dataProvider;
    }
}
