<?php

namespace app\modules\dashboard\models\ar;

use app\models\ar\Company;
use app\models\User;
use app\modules\system\behaviors\CompaniApiBehavior;
use app\modules\system\components\UserDateFormat;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "company_balance_operations".
 *
 * @property int $id
 * @property int $company_id
 * @property int $rule_id
 * @property int $type
 * @property int $inner_id
 * @property double $value
 * @property string $comment
 * @property string $created_at
 * @property string $user
 * @property bool $is_deleted
 * @property double $prev_balance
 */
class CompanyBalanceOperation extends \yii\db\ActiveRecord
{
    use UserDateFormat;

    const TYPE_IN = 1;
    const TYPE_OUT = 2;
    const TYPE_RULE_OUT = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dashboard.company_balance_operations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inner_id', 'company_id', 'rule_id', 'type',], 'integer'],
            ['value', 'double', 'min' => 0.01, 'tooSmall' => 'Значение «Сумма» должно быть больше 0.01'],
            [['comment', 'user'], 'string'],
            [['created_at',], 'safe'],
            [['is_deleted'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'type' => 'Операция',
            'value' => 'Сумма',
            'prev_balance' => 'Баланс до',
            'afterBalance' => 'Баланс после',
            'comment' => 'Комментарий',
            'created_at' => 'Дата',
            'user' => 'Создана',
        ];
    }

    public static function typeLabels()
    {
        return [
            self::TYPE_IN => 'Пополнение',
            self::TYPE_OUT => 'Списание',
            self::TYPE_RULE_OUT => 'Списание по правилу',
        ];
    }

    public static function find()
    {
        $tableName = static::tableName();
        return parent::find()->where([$tableName . '.is_deleted' => false]);
    }

    public static function getUsername($name, $surname, $project)
    {
        return "$name $surname ($project)";
    }

    public function behaviors()
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new Expression('NOW()'),
            ],
            'setUser' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'user',
                ],
                'value' => function () {
                    return \Yii::$app->dashboardUser->identity->username;
                },
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'is_deleted' => true
                ],
            ],
            'sendToProject' => [
                'class' => CompaniApiBehavior::className(),
            ],
        ];
    }

    public function saveWithCalculateBalance($runValidation = true, $attributeNames = null)
    {
        $result = false;

        if ($this->isNewRecord) {
            $transaction = Yii::$app->db->beginTransaction();

            try {
                $this->prev_balance = $this->company->balance;
                $result = parent::save($runValidation, $attributeNames);

                if (!$result) {
                    throw new \Exception();
                }

                if ($this->type == self::TYPE_IN) {
                    $this->company->balance = $this->company->balance + $this->value;
                }
                if ($this->type == self::TYPE_OUT || $this->type == self::TYPE_RULE_OUT) {
                    $this->company->balance = $this->company->balance - $this->value;
                }

                $this->company->detachBehavior('sendToProject');
                $result = $this->company->save();

                if ($result) {
                    $transaction->commit();
                }
            } catch (\Exception $e) {
                \Yii::error($e->getMessage(), 'Ошибка записи операции в баланс');
                $transaction->rollBack();

                return $result;
            }
        }

        return $result;
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        return parent::save($runValidation, $attributeNames);
    }

    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    public function getCompanyWithdrawalRule()
    {
        return $this->hasOne(CompanyWithdrawalRule::className(), ['id' => 'rule_id']);
    }

}