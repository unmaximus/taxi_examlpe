<?php

namespace app\modules\dashboard\models\ar;

use app\models\ar\Company;
use app\models\ar\Project;
use app\modules\system\helpers\ArrayHelper;
use Yii;
use app\modules\system\behaviors\CompaniApiBehavior;
use app\modules\system\components\UserDateFormat;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use app\modules\system\behaviors\PostgresArrayBehavior;

/**
 * This is the model class for table "company_withdrawal_rules".
 *
 * @property int $id
 * @property int $company_id
 * @property int $inner_id
 * @property string $name
 * @property int $type
 * @property bool $is_active
 * @property string $begin_datetime
 * @property string $end_datetime
 * @property double $value
 * @property string $comment
 * @property int $hour
 * @property int $every
 * @property integer[] params
 * @property integer[] additional_params
 */
class CompanyWithdrawalRule extends \yii\db\ActiveRecord
{
    use UserDateFormat;

    const DAILY = 1;
    const WEEKLY = 2;
    const MONTHLY = 3;

    public $daily_every = null;

    public $weekly_every = null;
    public $weekly_days = [];

    public $monthly_months = [];
    public $monthly_days = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dashboard.company_withdrawal_rules';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'begin_datetime', 'hour', 'type', 'value'], 'required'],
            [['inner_id', 'company_id', 'type', 'hour', 'every', 'updated_by'], 'integer'],
            [['is_active'], 'boolean'],
            [['begin_datetime', 'end_datetime', 'updated_at'], 'safe'],
            ['value', 'double', 'min' => 0.01, 'tooSmall' => 'Значение «Сумма» должно быть больше 0.01'],
            [['name'], 'string', 'max' => 128],
            [['comment'], 'string', 'max' => 512],
            [['daily_every', 'weekly_every'], 'integer'],
            [['begin_datetime', 'end_datetime'], 'default', 'value' => null],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => PostgresArrayBehavior::className(),
                'attributes' => ['params', 'additional_params'],
                'onEmptySaveNull' => true,
            ],
            'endDatetime' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'end_datetime',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'end_datetime',
                ],
                'value' => function ($even) {
                    return $even->sender->asDatetimeDb('end_datetime');
                },
            ],
            'beginDatetime' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'begin_datetime',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'begin_datetime',
                ],
                'value' => function ($even) {
                    return $even->sender->asDatetimeDb('begin_datetime');
                },
            ],
            'sendToProject' => [
                'class' => CompaniApiBehavior::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'begin_datetime' => 'Активировать с',
            'end_datetime' => 'Срок действия',
            'hour' => 'Во сколько часов',
            'type' => 'Повторять',
            'value' => 'Сумма',
            'comment' => 'Комментарий',
            'created_at' => 'Дата операции',
            'created_by' => 'Создана',
            'daily_every' => 'Через каждый, день',
            'weekly_every' => 'Через каждую, неделю',
            'weekly_days' => 'По дням',
            'monthly_months' => 'По месяцам',
            'monthly_days' => 'По дням',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery|Project
     */
    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id'])->via('company');
    }

    public function ruleDescription()
    {
        return $this->name . ' (' . $this->typeLabel() . ' - ' . $this->value . ')';
    }

    public static function typeLabels()
    {
        return [
            self::DAILY => 'Ежедневно',
            self::WEEKLY => 'Еженедельно',
            self::MONTHLY => 'Ежемесячно',
        ];
    }

    public function typeLabel()
    {
        $labels = self::typeLabels();
        return isset($labels[$this->type]) ? $labels[$this->type] : '';
    }

    public function loadRules($data, $formName='CompanyWithdrawalRule')
    {
        $data = isset($data[$formName]) ? $data[$formName] : $data;

        switch ($this->type) {
            case self::DAILY:
                $this->every = isset($data['daily_every'])
                    ? $data['daily_every'] : null;
                $this->params = null;
                $this->additional_params = null;
                break;
            case self::WEEKLY:
                $this->every = isset($data['weekly_every'])
                    ? $data['weekly_every'] : null;
                $this->params = isset($data['weekly_days'])
                    ? $data['weekly_days'] : null;
                $this->additional_params = null;
                break;
            case self::MONTHLY:
                $this->every = null;
                $this->params = isset($data['monthly_months'])
                    ? $data['monthly_months'] : null;
                $this->additional_params = isset($data['monthly_days'])
                    ? $data['monthly_days'] : null;
                break;
        }

        return 1;
    }

    public function afterFind()
    {
        parent::afterFind();

        switch ($this->type) {
            case self::DAILY:
                $this->daily_every = $this->every;
                break;
            case self::WEEKLY:
                $this->weekly_every = $this->every;
                $this->weekly_days = $this->params;
                break;
            case self::MONTHLY:
                $this->monthly_months = $this->params;
                $this->monthly_days = $this->additional_params;
                break;
        }
    }
}