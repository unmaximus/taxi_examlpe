<?php

namespace app\modules\dashboard\models\ar;

use app\models\ar\Company;
use app\models\ar\Project;
use app\modules\system\helpers\ArrayHelper;
use Yii;
use app\modules\system\behaviors\CompaniApiBehavior;

/**
 * This is the model class for table "company_limit_rules".
 *
 * @property int $id
 * @property int $inner_id
 * @property int $company_id
 * @property bool $is_active
 * @property int $type
 * @property int $limit
 * @property string $comment
 */
class CompanyLimitRule extends \yii\db\ActiveRecord
{
    const SCENARIO_API = 'api';

    const TYPE_VEHICLE = 1;
    const TYPE_USER = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dashboard.company_limit_rules';
    }

    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_API => ['type', 'limit', 'inner_id', 'company_id', 'is_active', 'comment'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'limit'], 'required'],
            [['id', 'inner_id', 'company_id', 'type', 'limit'], 'integer'],
            [['is_active'], 'boolean'],
            [['comment'], 'string', 'max' => 512],
            ['type', 'uniqueTypeVehicle', 'on' => self::SCENARIO_DEFAULT],
            ['type', 'uniqueTypeUser', 'on' => self::SCENARIO_DEFAULT],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'sendToProject' => [
                'class' => CompaniApiBehavior::className(),
            ],
        ];
    }


    /**
     * @return bool
     */
    public function uniqueTypeVehicle()
    {
        if ($this->type == self::TYPE_VEHICLE && isset($this->company_id)) {
            $query = CompanyLimitRule::find()->andWhere(['company_id' => $this->company_id])
                                     ->andWhere(['type' => $this->type]);

            if (!empty($this->id)) {
                $query->andWhere(['<>', 'id', $this->id]);
            }

            if ($query->count()) {
                $this->addError('type', 'Лимит этого типа уже существует');
                return false;
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function uniqueTypeUser()
    {
        if ($this->type == self::TYPE_USER && isset($this->company_id)) {
            $query = CompanyLimitRule::find()->andWhere(['company_id' => $this->company_id])
                                     ->andWhere(['type' => $this->type]);

            if (!empty($this->id)) {
                $query->andWhere(['<>', 'id', $this->id]);
            }

            if ($query->count()) {
                $this->addError('type', 'Лимит этого типа уже существует');
                return false;
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'is_active' => 'Статус',
            'type' => 'Тип',
            'limit' => 'Лимит',
            'comment' => 'Комментарий',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery|Project
     */
    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id'])->via('company');
    }

    /**
     * @return array
     */
    public static function typeLabels()
    {
        return [
            self::TYPE_VEHICLE => 'по ТС',
            self::TYPE_USER => 'по пользователям',
        ];
    }

    /**
     * @return mixed|string
     */
    public function typeLabel()
    {
        $labels = self::typeLabels();
        return isset($labels[$this->type]) ? $labels[$this->type] : '';
    }
    /**
     * @return string
     */
    public function ruleDescription()
    {
        $result =  $this->typeLabel();
        $result .= " - " . $this->limit;

        return $result;
    }
}