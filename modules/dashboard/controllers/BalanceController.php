<?php

namespace app\modules\dashboard\controllers;

use app\models\ar\Company;
use Yii;
use app\modules\dashboard\models\ar\CompanyBalanceOperation;
use app\modules\dashboard\models\search\BalanceSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BalanceController implements the CRUD actions for CompanyBalanceOperation model.
 */
class BalanceController extends DashboardBaseController
{

	/**
	 * @param null $id
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 */
	public function actionIndex($id = null)
    {
        $searchModel = new BalanceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

	    $model = Company::findOne($id);
	    if ($model === null) {
		    throw new NotFoundHttpException('The requested page does not exist.');
	    }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Finds the CompanyBalanceOperation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompanyBalanceOperation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CompanyBalanceOperation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
