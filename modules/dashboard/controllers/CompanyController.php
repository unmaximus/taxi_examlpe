<?php

namespace app\modules\dashboard\controllers;

use app\modules\dashboard\models\ar\CompanyBalanceOperation;
use app\modules\dashboard\models\ar\CompanyLimitRule;
use app\modules\dashboard\models\ar\CompanyWithdrawalRule;
use app\models\ar\Project;
use app\modules\system\behaviors\AjaxMethodFilter;
use app\modules\system\helpers\ArrayHelper;
use app\modules\system\widgets\ActiveForm;
use Yii;
use app\models\ar\Company;
use app\modules\dashboard\models\search\CompanySearch;
use yii\db\ActiveRecord;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends DashboardBaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
	    return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
		    'ajaxMethod' => AjaxMethodFilter::className(),
        ]);
    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

	    if (Yii::$app->request->isAjax) {
		    return $this->renderAjax('_grid', [
			    'searchModel' => $searchModel,
			    'dataProvider' => $dataProvider,
		    ]);
	    }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	/**
	 * @param $id
	 *
	 * @return mixed|string
	 * @throws NotFoundHttpException
	 * @throws \yii\base\InvalidRouteException
	 */
	public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
	        return $this->runAction('index');
        }

        return $this->renderAjax('_company-form', [
            'model' => $model,
        ]);
    }

	/**
	 * @param $id
	 *
	 * @return string
	 * @throws BadRequestHttpException
	 * @throws NotFoundHttpException
	 * @throws \yii\base\ExitException
	 */
	public function actionCreateWithdrawalRule($id)
	{
		$model = $this->findModel($id);

		$modelWithdrawal = new CompanyWithdrawalRule([
			'company_id' => $id,
		]);

		$post = Yii::$app->request->post();

		if ($modelWithdrawal->load($post) && isset($modelWithdrawal->type)) {
			if (empty($post['CompanyWithdrawalRule']['is_active'])) {
				$modelWithdrawal->is_active = false;
			}
			$modelWithdrawal->loadRules($post);

			if ($modelWithdrawal->save()) {
				return $this->renderAjax('_withdrawal-rule-description', [
					'models' => $model->companyWithdrawalRule,
				]);
			} else {
				Yii::$app->response->statusCode = 500;
				Yii::$app->end();
			}
		}

		return $this->renderAjax('_company-withdrawal-rule-form', [
			'model' => $modelWithdrawal,
		]);
	}

	/**
	 * @param $id
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 * @throws \yii\base\ExitException
	 */
	public function actionUpdateWithdrawalRule($id)
	{
		$modelWithdrawal = $this->findWithdrawalRuleModel($id);
		$model = $this->findModel($modelWithdrawal->company_id);

		$post = Yii::$app->request->post();

		if ($modelWithdrawal->load($post)) {
			if (empty($post['CompanyWithdrawalRule']['is_active'])) {
				$modelWithdrawal->is_active = false;
			}
			$modelWithdrawal->loadRules($post);

			if ($modelWithdrawal->save()) {
				return $this->renderAjax('_withdrawal-rule-description', [
					'models' => $model->companyWithdrawalRule,
				]);
			} else {
				Yii::$app->response->statusCode = 500;
				Yii::$app->end();
			}
		}

		return $this->renderAjax('_company-withdrawal-rule-form', [
			'model' => $modelWithdrawal,
		]);
	}

	/**
	 * @param $id
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\base\ExitException
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDeleteWithdrawalRule($id)
	{
		$modelWithdrawal = $this->findWithdrawalRuleModel($id);
		$model = $this->findModel($modelWithdrawal->company_id);

		if ($modelWithdrawal->delete()) {
			$resultHtml =  $this->renderAjax('_withdrawal-rule-description', [
				'models' => $model->companyWithdrawalRule,
			]);

			if (empty($resultHtml)) {
				Yii::$app->response->statusCode = 201;
			}

			return $resultHtml;
		} else {
			Yii::$app->response->statusCode = 500;
			Yii::$app->end();
		}
	}

	/**
	 * @param $id
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 * @throws \yii\base\ExitException
	 */
	public function actionCreateLimitRule($id)
	{
		$model = $this->findModel($id);

		$modelLimit = new CompanyLimitRule([
			'company_id' => $id,
			'is_active' => true,
		]);

		$post = Yii::$app->request->post();

		if ($modelLimit->load($post) && isset($modelLimit->type)) {
			if (empty($post['CompanyLimitRule']['is_active'])) {
				$modelLimit->is_active = false;
			}

			if ($modelLimit->save()) {
				return $this->renderAjax('_limit-rule-description', [
					'models' => $model->companyLimitRule,
				]);
			} else {
				Yii::$app->response->statusCode = 500;
				Yii::$app->end();
			}
		}

		return $this->renderAjax('_company-limit-rule-form', [
			'model' => $modelLimit,
		]);
	}


	/**
	 * @param $id
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 * @throws \yii\base\ExitException
	 */
	public function actionUpdateLimitRule($id)
	{

		$modelLimit = $this->findLimitRuleModel($id);
		$model = $this->findModel($modelLimit->company_id);

		$post = Yii::$app->request->post();

		if ($modelLimit->load($post) && isset($modelLimit->type)) {
			if (empty($post['CompanyLimitRule']['is_active'])) {
				$modelLimit->is_active = false;
			}

			if ($modelLimit->save()) {
				return $this->renderAjax('_limit-rule-description', [
					'models' => $model->companyLimitRule,
				]);
			} else {
				Yii::$app->response->statusCode = 500;
				Yii::$app->end();
			}
		}

		return $this->renderAjax('_company-limit-rule-form', [
			'model' => $modelLimit,
		]);
	}

	/**
	 * @param $id
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\base\ExitException
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDeleteLimitRule($id)
	{
		$modelLimit = $this->findLimitRuleModel($id);
		$model = $this->findModel($modelLimit->company_id);

		if ($modelLimit->delete()) {
			$resultHtml =  $this->renderAjax('_limit-rule-description', [
				'models' => $model->companyLimitRule,
			]);

			if (empty($resultHtml)) {
				Yii::$app->response->statusCode = 201;
			}

			return $resultHtml;
		} else {
			Yii::$app->response->statusCode = 500;
			Yii::$app->end();
		}
	}

	/**
	 * @param $id
	 *
	 * @return mixed|string
	 * @throws \yii\base\ExitException
	 * @throws \yii\base\InvalidRouteException
	 */
	public function actionCreateOperation($id)
	{

		$model = new CompanyBalanceOperation();
		$model->company_id = $id;

		$post = Yii::$app->request->post();

		if ($model->load($post)) {
			if ($model->save()) {
				return $this->runAction('index');
			} else {
				Yii::$app->response->statusCode = 500;
				Yii::$app->end();
			}
		}

		return $this->renderAjax('_company-balance-operation-form', [
			'model' => $model,
		]);
	}

	/**
	 * Ajax validation for all form
	 *
	 * @throws \yii\base\ExitException
	 */
	public function ajaxValidation()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$request = Yii::$app->request;
		/** @var ActiveRecord $model */

		$modelName = $request->getQueryParam('model');

		if (!isset($modelName)) {
			Yii::$app->response->content = json_encode([]);
			Yii::$app->end();
		}

		$model = new $modelName;

		if (in_array('validation', array_keys($model->scenarios()))) {
			$model->scenario = 'validation';
		}

		if ($request->getBodyParam('ajax') && $model->load($request->post())) {
			Yii::$app->response->content = json_encode(ActiveForm::validate($model));
			Yii::$app->end();
		}

		Yii::$app->response->content = json_encode([]);
		Yii::$app->end();
	}

	/**
	 * @param $id
	 *
	 * @return Company|null
	 * @throws NotFoundHttpException
	 */
	protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


	/**
	 * @param $id
	 *
	 * @return CompanyLimitRule|null
	 * @throws NotFoundHttpException
	 */
	protected function findLimitRuleModel($id)
    {
        if (($model = CompanyLimitRule::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


	/**
	 * @param $id
	 *
	 * @return CompanyWithdrawalRule|null
	 * @throws NotFoundHttpException
	 */
	protected function findWithdrawalRuleModel($id)
    {
        if (($model = CompanyWithdrawalRule::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
