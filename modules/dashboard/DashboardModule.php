<?php

namespace app\modules\dashboard;

use Yii;
use yii\base\BootstrapInterface;
use yii\base\Module;

/**
 * dashboard module definition class
 */
class DashboardModule extends Module implements BootstrapInterface
{
    
    /**
     * @var string путь к директории с web-приложениями
     */
    public $baseAppsPath;
    
    /**
     * @var string путь к крон файлу
     */
    public $cronPath;
    
    /**
     * @inheritdoc
     */
    public $defaultRoute = 'company';
    
    
    public $layout = 'blank';
    
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\dashboard\controllers';
    
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->setLayoutPath(Yii::getAlias('@app/views/layouts'));
        Yii::configure($this, require(Yii::getAlias('@app/config/dashboard.php')));
    }

    public function bootstrap($app) {

        if ($app instanceof \yii\console\Application)
            $this->controllerNamespace = 'app\modules\dashboard\console';
    }
    
}